//
//  ViewController.m
//  iBeaconCentral
//
//  Created by FUJITA YASUO on 2014/07/15.
//  Copyright (c) 2014年 FUJITA YASUO. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController ()

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSUUID *proximityUUID;
@property (nonatomic) CLBeaconRegion *beaconRegion;

@end

@implementation ViewController

- (void)viewDidLoad
{
  [super viewDidLoad];

  //BLE対応の有無の確認
  if ([CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
    // CLLocationManagerの生成とデリゲートの設定
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    
    // 生成したUUIDからNSUUIDを作成
    self.proximityUUID = [[NSUUID alloc] initWithUUIDString:@"00000000-98EF-1001-B000-001C4D9A7C81"];
    // CLBeaconRegionを作成
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:self.proximityUUID
                                                           identifier:@"com.mettoboshi.testregion"];
    
//    Default Value
//    self.beaconRegion.notifyOnEntry = YES;
//    self.beaconRegion.notifyOnExit = YES;
//    self.beaconRegion.notifyEntryStateOnDisplay = NO;

    self.beaconRegion.notifyEntryStateOnDisplay = YES;


    // Beaconによる領域観測を開始
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
    
  } else {
    // TBD ハードが対応してないよとメッセージを出す。
  }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
  [self.locationManager requestStateForRegion:self.beaconRegion];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
  [self sendLocalNotificationForMessage:@"Enter Region"];
//  NSLog(@"Enter Region");
  
  if ([region isMemberOfClass:[CLBeaconRegion class]] && [CLLocationManager isRangingAvailable]) {
    [self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
  }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
  [self sendLocalNotificationForMessage:@"Exit Region"];
//  NSLog(@"Exit Region");

  if ([region isMemberOfClass:[CLBeaconRegion class]] && [CLLocationManager isRangingAvailable]) {
    [self.locationManager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
  }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
  if (beacons.count > 0) {
    CLBeacon *nearestBeacon = beacons.firstObject;
    
    NSString *rangeMessage;
    
    switch (nearestBeacon.proximity) {
      case CLProximityImmediate:
        rangeMessage = @"Range Immediate: ";
        break;
      case CLProximityNear:
        rangeMessage = @"Range Near: ";
        break;
      case CLProximityFar:
        rangeMessage = @"Range Far: ";
        break;
      default:
        rangeMessage = @"Range Unknown: ";
        break;
    }
    
    NSString *message = [NSString stringWithFormat:@"major:%@, minor:%@, accuracy:%f, rssi:%d",
                         nearestBeacon.major, nearestBeacon.minor, nearestBeacon.accuracy, nearestBeacon.rssi];
//    [self sendLocalNotificationForMessage:[rangeMessage stringByAppendingString:message]];
    NSLog(@"%@",message);
    NSLog(@"%@",rangeMessage);
  }
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
  [self sendLocalNotificationForMessage:@"Exit Region"];
}

#pragma mark - Private methods

- (void)sendLocalNotificationForMessage:(NSString *)message
{
  UILocalNotification *localNotification = [UILocalNotification new];
  localNotification.alertBody = message;
  localNotification.fireDate = [NSDate date];
  localNotification.soundName = UILocalNotificationDefaultSoundName;
  [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
  switch (state) {
    case CLRegionStateInside: // リージョン内にいる
      if ([region isMemberOfClass:[CLBeaconRegion class]] && [CLLocationManager isRangingAvailable]) {
        [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
      }
      break;
    case CLRegionStateOutside:
    case CLRegionStateUnknown:
    default:
      break;
  }
}

@end
